//! # Eloclub del siglo XXI
//!
//! Despues de 25 años se vuelve a retomar esta aplicacion, que en un principio se desarrollo en
//! Dbase 4.
//! Ahora se va a desarrollar en Rust y SQl
//!
//! El plan de desarrollo,
//! 1. Crear el calculo individial de dos rivales.
//! 2. Base de datos de los jugadores con diversos identificadores, o numero de socio o Nick.
//! 3. Base de datos de las partidas realizadas. Por tipo juego (Bala, Blitz, Rapid, Classic,
//!    Standard)
//! 4. Base de datos de Historial de variaciones de Elo.
//!
//! Antiguamente la potencia de los ordenadores no era muy buena y tradaba bastante en calcularse.
//! Ahora, hay que ver, si conviene que lo recalcule todo desde el principio o desde un puento de
//! partida.
//! Defino en la Estuctura Elo La probabilidad entre diferencia de puntos    

//! https://handbook.fide.com/chapter/B022022
//! https://handbook.fide.com/chapter/B02RBRegulations2022
//!
//!
//! 8.2 Determinar la calificación inicial 'Ru' de un jugador.

//!     8.2.1 Si un jugador no calificado obtiene cero en su primer evento, este puntaje no se tiene en cuenta. De lo contrario, su calificación se calcula utilizando todos sus resultados como en 7.1.4.
//!     8.2.2 Ra es la calificación promedio de los oponentes calificados del jugador.
//!     8.2.3 Si el jugador anota 50%, entonces Ru = Ra.
//!         Si obtienen más del 50%, entonces Ru = Ra + 20 por cada medio punto obtuvo más del 50%.
//!         Si obtienen menos del 50%, entonces Ru = Ra + dp
//!         Ru se redondea al número entero más cercano.

//! 8.2.4 Si un jugador no calificado recibe una calificación publicada antes de que se califique un torneo en particular en el que ha jugado, entonces se clasifica como un jugador calificado con su calificación actual, pero en la calificación de sus oponentes se cuentan como un jugador no calificado .
//!
//!
// Hay que recordar que hay que poner las ependencia en el archivo Cargo.toml
// Ru (ru) = Rating Unrated .
// Ra (ra) = Rating Average rating of rated opponents in all tournaments.
// W (w) - Total score against rated opponents in all tournaments.
// N (n)- Number of games against rated opponents in all tournaments.
// p (p) - Percentage en numeros decimales
// dp (dp) - Diferencias de Rating
#![allow(warnings)] 
use std::io;
use rated::*;
use unrated::*;
// use rust_decimal::prelude::*;

fn main() {
    clearscreen::clear().unwrap();
    println!("\n\n{: >25}{:->25} Elo Club XXI {:-<25}", "", "", "");

    loop {
        println!("{: >25} ¿Tiene Rating? (s/n)","");

        let mut rating = String::new();

        io::stdin()
            .read_line(&mut rating)
            .expect("Error al leer la entrada");

        let rating = rating.trim().to_lowercase();

        if rating == "s" {
            println!("{: >25} Tiene Rating. Calculando su nuevo rating.","");
            rated::rated();

            break;
        } else if rating == "n" {
            println!("{: >25} No tiene Rating...Calculando entonces uno nuevo.","");
            unrated::unrated();
            break;
        } else {
            println!("{: >25}Respuesta inválida. Por favor, ingresa 's' o 'n'.","");
        }
    }
}
