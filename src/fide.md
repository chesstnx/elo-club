# FIDE Rating Regulations effective from 1 January 2022

#  FIDE RATING REGULATIONS (Approved by FIDE Council on 27/10/2021)
 
 Applied from 1 January, 2022
 
## 0.       Introduction
 
### 0.1     The following regulations may be altered by the FIDE Council upon recommendation of the Qualification Commission (QC). For tournaments, changes will apply to those starting on or after the date upon which they become effective.
 
### 0.2     The tournaments to be rated shall be pre-registered by the federation in whose territory it is held and they will be responsible for the submission of results and rating fees.  Council may additionally designate these rights and responsibilities to Affiliated Organisations that are representing an autonomous territory which is contained within no more than one Federation.
 
 The tournament and its playing schedule must be registered three days before the tournament starts. The QC Chairman may refuse to register a tournament. He may also allow a tournament to be rated even though it has been registered less than three days before the tournament starts.
 
 All tournaments played under Hybrid conditions as described in 2.1 must be approved individually by the QC Chairman.
 
### 0.3     Tournament reports for all official FIDE and Continental events must be submitted and shall be rated. The Chief Arbiter is responsible for submitting the rating report file to the FIDE Rating Administrator.
 
### 0.4     FIDE reserves the right not to rate a specific tournament. The organiser of the tournament has the right to appeal to the QC. Such an appeal must be made within seven days of the communication of the decision.
 
 1.       Rate of Play
 
 1.1     For a game to be rated each player must at the start of the tournament have the following minimum periods in which to complete all the moves, assuming the game lasts 60 moves.
 
 Where at least one of the players in the game has a rating of 2400 or higher, each player must have a minimum of 120 minutes.
 
 Where at least one of the players in the game has a rating 1800 or higher, each player must have a minimum of 90 minutes.
 
 Where both of the players in the game are rated below 1800, each player must have a minimum of 60 minutes.
 
 1.2    Where a certain number of moves is specified in the first time control, it shall be at least 30 moves.
 
 2.       Laws to be Followed
 
 2.1     Play shall be governed by the FIDE Laws of Chess or the Regulations for Hybrid Chess Competitions (Part IIIb within the FIDE Online Chess Regulations).
 
 3.       Hours of Play
 
 3.1     There must be no more than 12 hours play in one day. This is calculated based on games that last 60 moves, although games played using increments may last longer.
 
 4.       Reporting Frequency
 
 4.1     For tournaments lasting more than 30 days, interim results must be reported on a monthly basis.
 
 5.       Unplayed Games         
 
 5.1     Whether these occur because of forfeiture or any other reason, they are not counted. Except in case of force majeure, any game where both players have made at least one move will be rated, unless the regulations relating to Fair Play require otherwise.
 
 6.       Matches                       
 
 6.1     Matches in which one player is unrated shall not be rated.
 
 6.2     Where a match is over a specific number of games, those played after one player has won shall not be rated.  This requirement may be waived by prior request.
 
 7.       Official FIDE Rating List
 
 7.1     On the first day of each month, FIDE shall prepare a list which incorporates all rated play during the rating period into the previous list. This shall be done using the rating system formula.
 
 7.1.1      The rating period (for new players, see 7.1.4) is the period where a certain rating list is valid.
 
 7.1.2      The following data will be published concerning each player whose rating is at least 1000 as of the current list: FIDE title, Federation, Current Rating, ID Number, Number of games rated in the rating period, Year of Birth, Gender and the current value of K for the player.
 
 7.1.3      The closing date for tournaments for a list is 3 days before the date of the list; tournaments ending before or on that day may be rated on the list. Official FIDE events may be rated on the list even if they end on the last day before the list date.
 
 7.1.4      A rating for a player new to the list shall be published when it is based on 5 games against rated opponents. This need not be met in one tournament. Results from other tournaments played within consecutive rating periods of not more than 26 months are pooled to obtain the initial rating. The rating must be at least 1000.
 
 7.2     Players who are not to be included on the list or to be shown as inactive:
 
 7.2.1      Players whose ratings drop below 1000 are shown as unrated on the next list. Thereafter they are treated in the same manner as any other unrated player.
 
 7.2.2      Players listed as active:
 
 A player is considered to commence inactivity if they play no rated games in a one-year period.     
 A player regains their activity if they play at least one rated game in a period.  They are then listed as active on the next list.
 8.       The working of the FIDE Rating System        
 
 The FIDE Rating system is a numerical system in which fractional scores are converted to rating differences and vice versa. Its function is to produce measurement information of the best statistical quality.
 
 8.1     The rating scale is an arbitrary one with a class interval set at 200 points. The tables that follow show the conversion of fractional score 'p' into rating difference 'dp'. For a zero or 1.0 score dp is necessarily indeterminate but is shown notionally as 800. The second table shows conversion of difference in rating 'D' into scoring probability 'PD' for the higher 'H' and the lower 'L' rated player respectively. Thus, the two tables are effectively mirror-images.
 
 8.1.1      The table of conversion from fractional score, p, into rating differences, dp
 
 p	dp	p	dp	p	dp	p	dp	p	dp	p	dp
 1.0	800	.83	273	.66	117	.49	-7	.32	 -133	.15	-296
 .99	677	.82	262	.65	110	.48	-14	.31	 -141	.14	-309
 .98	589	.81	251	.64	102	.47	-21	.30	 -149	.13	-322
 .97	538	.80	240	.63	95	.46	-29	.29	 -158	.12	-336
 .96	501	.79	230	.62	87	.45	-36	.28	 -166	.11	-351
 .95	470	.78	220	.61	80	.44	-43	.27	 -175	.10	-366
 .94	444	.77	211	.60	72	.43	-50	.26	 -184	.09	-383
 .93	422	.76	202	.59	65	.42	-57	.25	 -193	.08	-401
 .92	401	.75	193	.58	57	.41	-65	.24	 -202	.07	-422
 .91	383	.74	184	.57	50	.40	-72	.23	 -211	.06	-444
 .90	366	.73	175	.56	43	.39	-80	.22	 -220	.05	-470
 .89	351	.72	166	.55	36	.38	-87	.21	 -230	.04	-501
 .88	336	.71	158	.54	29	.37	-95	.20	 -240	.03	-538
 .87	322	.70	149	.53	21	.36	-102	.19	 -251	.02	-589
 .86	309	.69	141	.52	14	.35	-110	.18	 -262	.01	-677
 .85	296	.68	133	.51	7	.34	-117	.17	 -273	.00	-800
 .84	284	.67	125	.50	0	.33	-125	.16	 -284	 	 
 8.1.2      Table of conversion of difference in rating, D, into scoring probability PD, for the higher, H, and the lower, L, rated player respectively.
 
 D	PD	D	PD	D	PD	D	PD
 Rtg Dif	H	L	Rtg Dif	H	L	Rtg Dif	H	 L	Rtg Dif	H	L
 0-3	.50	.50	92-98	.63	.37	 198-206	.76	.24	345-357	.89	.11
 4-10	.51	.49	99-106	.64	.36	 207-215	.77	.23	358-374	.90	.10
 11-17	.52	.48	107-113	.65	.35	 216-225	.78	.22	375-391	.91	.09
 18-25	.53	.47	114-121	.66	.34	 226-235	.79	.21	392-411	.92	.08
 26-32	.54	.46	122-129	.67	.33	 236-245	.80	.20	412-432	.93	.07
 33-39	.55	.45	130-137	.68	.32	 246-256	.81	.19	433-456	.94	.06
 40-46	.56	.44	138-145	.69	.31	 257-267	.82	.18	457-484	.95	.05
 47-53	.57	.43	146-153	.70	.30	 268-278	.83	.17	485-517	.96	.04
 54-61	.58	.42	154-162	.71	.29	 279-290	.84	.16	518-559	.97	.03
 62-68	.59	.41	163-170	.72	.28	 291-302	.85	.15	560-619	.98	.02
 69-76	.60	.40	171-179	.73	.27	 303-315	.86	.14	620-735	.99	.01
 77-83	.61	.39	180-188	.74	.26	 316-328	.87	.13	> 735	1.0	.00
 84-91	.62	.38	189-197	.75	.25	 329-344	.88	.12	 	 	 
 8.2     Determining the initial rating 'Ru' of a player.
 
 8.2.1      If an unrated player scores zero in their first event this score is disregarded. Otherwise, their rating is calculated using all their results as in 7.1.4.
 
 8.2.2      Ra is the average rating of the player's rated opponents.
 
 8.2.3      If the player scores 50%, then Ru = Ra.
 
 If they score more than 50%, then Ru = Ra + 20 for each half point scored over 50%.
 
 If they score less than 50%, then  Ru = Ra + dp
 
 Ru is rounded to the nearest whole number.
 
 8.2.4      If an unrated player receives a published rating before a particular tournament in which they have played is rated, then they are rated as a rated player with their current rating, but in the rating of their opponents they are counted as an unrated player.
 
 8.3     Determining the rating change for a rated player
 
 8.3.1      For each game played against a rated player, determine the difference in rating between the player and their opponent, D.
 
 A difference in rating of more than 400 points shall be counted for rating purposes as though it were a difference of 400 points.  In any tournament, a player may benefit from only one upgrade under this rule, for the game in which the rating difference is greatest.
 
 8.3.2      a) Use table 8.1.2 to determine the player's score probability PD for each game.
 
 b) Delta R = score - PD.  For each game, the score is 1, 0.5 or 0.
 
 c) Sigma Delta R = the sum of Delta Rs for a tournament or Rating Period.
 
 d) Sigma Delta R x K = the Rating Change for a tournament or Rating Period.
 
 8.3.3      K is the development coefficient.
 
 K = 40 for a player new to the rating list until they have completed events with at least 30 games.
 
 K = 20 as long as a player's rating remains under 2400.
 
 K = 10 once a player's published rating has reached 2400 and remains at that level subsequently, even if the rating drops below 2400.
 
 K = 40 for all players until the end of the year of their 18th birthday, as long as their rating remains under 2300.
 
 If the number of games (n) for a player on any list for a rating period multiplied by K (as defined above) exceeds 700, then K shall be the largest whole number such that K x n does not exceed 700.
 
 8.3.4      The Rating Change for a Rating Period is rounded to the nearest whole number. 0.5 is rounded away from zero.
 
 9.       Reporting Procedures
 
 9.1     The Chief Arbiter of a FIDE registered tournament must provide the tournament report (TRF file) to the Rating Officer of the federation where the tournament took place.
 
 Once satisfied that the tournament was conducted in accordance with all relevant FIDE Regulations, the Rating Officer shall be responsible for uploading the TRF file to the FIDE Rating Server.  This should be done in time for the tournament to be rated in the monthly list in which the tournament is registered or, if there are five days or less from the last day of the tournament to the end of the month, for the following list.
 
 If the tournament report is not submitted in time to be included in the third rating list after it ends, the tournament will not be rated.
 
 9.2     The regulations of a rated event must make clear that it will be rated.
 
 9.3     Each national federation shall designate a Rating Officer to coordinate and expedite qualification and rating matters. Their name and details must be given to the FIDE Secretariat.
 
 9.4     For Hybrid events, full pgn files must be submitted with the tournament report.
 
 10.     Inclusion in the Rating list     
 
 10.1   To be included in the FIDE Rating List, a player must be registered through a national chess federation which is a member of FIDE, unless otherwise approved by FIDE Council. The Federation must not be temporarily or permanently excluded from membership.
 
 10.2   It is the responsibility of the federation to report deaths of their players to FIDE.
 
 Login or password is incorrect
 Email incorrect We have sent you an email with link. Please use this link for your account
 A. Administrative Subjects
 01. Non-Elected Commissions
 02. Financial Regulations
 01. International Title Regulations (Qualification Commission)
 02. FIDE Rating Regulations (Qualification Commission)
 03. Regulations on Registration & Licensing of Players
 04. Registration, Transfer & Rules of Eligibility for Player
 06. Regulations for the Titles of Arbiters
 11. FIDE Online Arena Regulations
 C. General Rules and Technical Recommendations for Tournaments
 D. Regulations for Specific Competitions
 01. FIDE Individual World Championship Cycles
 03. Cycle for Competitions for Men`s and Women`s National Teams
 04. Other FIDE Competitions
 07. Regulations for the FIDE World Rapid & Blitz Championships
