# Elo Club 

# Eloclub del siglo XXI

Despues de 25 años se vuelve a retomar esta aplicacion, que en un principio se desarrollo en
Dbase 4.
Ahora se va a desarrollar en Rust y SQl

El plan de desarrollo,
1. Crear el calculo individial de dos rivales.
2. Base de datos de los jugadores con diversos identificadores, o numero de socio o Nick.
3. Base de datos de las partidas realizadas. Por tipo juego (Bala, Blitz, Rapid, Classic,
   Standard)
4. Base de datos de Historial de variaciones de Elo.

Antiguamente la potencia de los ordenadores no era muy buena y tradaba bastante en calcularse.
Ahora, hay que ver, si conviene que lo recalcule todo desde el principio o desde un puento de
partida.
Defino en la Estuctura Elo La probabilidad entre diferencia de puntos    

https://handbook.fide.com/chapter/B022022


8.2 Determinar la calificación inicial 'Ru' de un jugador.

    8.2.1 Si un jugador no calificado obtiene cero en su primer evento, este puntaje no se tiene en cuenta. De lo contrario, su calificación se calcula utilizando todos sus resultados como en 7.1.4.
    8.2.2 Ra es la calificación promedio de los oponentes calificados del jugador.
    8.2.3 Si el jugador anota 50%, entonces Ru = Ra.
        Si obtienen más del 50%, entonces Ru = Ra + 20 por cada medio punto obtuvo más del 50%.
        Si obtienen menos del 50%, entonces Ru = Ra + dp
        Ru se redondea al número entero más cercano.

    8.2.4 Si un jugador no calificado recibe una calificación publicada antes de que se califique un torneo en particular en el que ha jugado, entonces se clasifica como un jugador calificado con su calificación actual, pero en la calificación de sus oponentes se cuentan como un jugador no calificado .

