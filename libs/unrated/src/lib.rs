use std::io;
use tables::*;

pub fn unrated() {
    let mut input = String::new();
    println!("{: >25}Numero de partidas disputadas: ","");
    io::stdin()
        .read_line(&mut input)
        .expect("No se pudo leer la entrada");

    let n: u32 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => panic!("No se pudo convertir la entrada a un número entero"),
    };

    let mut result = String::new();
    println!("{: >25}Puntos Obtenidos: ","");
    io::stdin()
        .read_line(&mut result)
        .expect("No se pudo leer la entrada");

    let w: f32 = match result.trim().parse() {
        Ok(num) => num,
        Err(_) => panic!("No se pudo convertir la entrada a un número decimal"),
    };

    println!("{: >25}{} Partidas con un resultado de: {}","", n, w);

    // Definicion del vector con los Elos de las difentes partidas: n
    // No hace fatla introducir resultado de cada partida, ya que es una media y obtiene el rendimiento.
    // Para cuando ya se tenga Elo, si se tendra que tener en cuenta el resultado individualizado por cada partida.
    n as usize;

    let mut elos: Vec<u32> = Vec::new();

    for i in 0..n {
        println!("{: >25}Elo Jugador {}: ","", i + 1);
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("No se pudo leer la entrada");

        let elo: u32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => panic!("No se pudo convertir la entrada a un número decimal"),
        };

        elos.push(elo); // agregamos el valor al vector
                        //       println!("{: >25}Linea {}, ","", i);
    }
    println!("{: >25}Los Elos de los oponentes son: {:?}","", elos); // imprimimos el vector completo

    let n = elos.len(); // obtenemos la cantidad de elementos del vector

    let suma: u32 = elos.iter().sum(); // sumamos todos los valores del vector

    let ra = suma / n as u32; // calculamos la media dividiendo la suma entre la cantidad de elementos

    println!("{: >25}La media de los valores es: {}","", ra);
    // Verificar si los puntos estan por debajo del 50% igual al 50% o por encima del 50%
    let num: f32 = n.clone() as f32;
    let p: f32;
    let p_rnd = format!("{:.2}", w / num); // n: Numero de Partidas
    p = p_rnd.parse().unwrap();
    println!("{: >25}Porcentaje obtenido: {}","", p);

    // Igual al 50%
    if p == 0.5 {
        println!("{: >25}Ru Rating Inicial es: {}","", ra)

    // Superior al 50%a
    } else if p > 0.5 {
        println!("{: >25}Superior al  50%","");

        // Determinamos cuantos medio puntos de mas ha obtenido
        // La mitad de rondas r2
        let halfs: f32;
        halfs = (w as f32 - (n as f32 / 2.0) as f32) / 0.5;

        println!("{: >25} n_f32 es {}, w_f32 es {}, halfs son {}","", n, w, halfs);
        let inc: u32 = 20 * halfs as u32;

        println!("{: >25}El Incremento de medios puntos es : {}","", inc);
        let ru = ra + inc;

        println!("{: >25}Ru Rating Inicial es: {}","", ru)

    // Inferior al 50%
    } else if p < 0.5 {
        println!("{: >25}Inferior 50%","");

        println!("{: >25}Rating Average: {} (ra)","", ra);
        println!("{: >25}Porcentaje obtenido: {}","", p);

        let inc: i32 = match p2dp(p) {
            Some(dp) => dp,
            None => 0,
            // println!("{: >25}No se encontró un valor de dp para p={}","", p),
        };
        let ru: i32 = ra as i32 + inc as i32;
        println!(
            "Games (n): {}, Score (w): {}, Rating Average (ra): {}, Percentage (p): {}, Rating Diference (dp): {} , Inc (inc): {}, Rating Unrated (ru) [ {ra} {inc} ] = {}",
            n, w, ra, p, inc, inc,  ru
        );
    }
}
