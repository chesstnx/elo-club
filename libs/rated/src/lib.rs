use std::io;
// use clearscreen::*;


pub fn rated() {
// clearscreen::clear().unwrap();
    println!("{: >25} Area de calculo de nuevo rating","");
    // R = Rating del jugador, puesto en minuscula para Rust
    let mut input = String::new();
    println!("{: >25}Rating Inicial (ELO):","");
    // R );
    io::stdin()
        .read_line(&mut input)
        .expect("No se pudo leer la entrada");

    let r: u32 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => panic!("No se pudo convertir la entrada a un número entero"),
    };
    // n = Numero de partidas disputadas
    let mut input = String::new();
    println!("{: >25}Numero de partidas disputadas: ","");
    // R );
    io::stdin()
        .read_line(&mut input)
        .expect("No se pudo leer la entrada");

    let n: u32 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => panic!("No se pudo convertir la entrada a un número entero"),
    };
    //println!("{: >25}Tu ELO Inicial es: {}","", r);
    //println!("{: >25}Numero de partidas jugadas {}","", n);

    // Posteriormente crear funcion de recolector de jugadores a vector.
    let mut elos: Vec<i32> = Vec::new();
    let mut elos_diff: Vec<i32> = Vec::new(); //Vector de las diferencias de ELO
    let mut elos_score: Vec<f32> = Vec::new(); //Vector de los puntos de ELO
    let mut elos_coef: Vec<f32> = Vec::new(); //Vector de los coeficientes de Elo
    let mut elos_inc: Vec<f32> = Vec::new(); //Vector de los Incrementos de Elo
//   let mut elo_end: Vec<u32>; // Elo Final

    for i in 0..n {
        println!("{: >25}Elo Jugador {}: ","", i + 1);
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("No se pudo leer la entrada");

        let elo: i32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => panic!("No se pudo convertir la entrada a un número decimal"),
        };

        elos.push(elo); // agregamos el valor al vector
                        // Y creamos la difencia respecto a ese valor

        //        println!("{: >25}Rating [r]: {:?}", r); // imprimimos el vector completo
        let diff: i32 = elo as i32 - r as i32 ;
        elos_diff.push(diff);

        println!("{: >25}Puntuacion obtenida [score]: {}: ","", i + 1);
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("No se pudo leer la entrada");

        let score: f32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => panic!("No se pudo convertir la entrada a un número decimal"),
        };

        elos_score.push(score); // agregamos el valor al vector
                                // Y creamos la difencia respecto a ese valor
                                //        Obtenemos el factor de Increment/Decreemento de Elo [elos_coef]
         
        //println!("{: >25}elo_diff: {:?}","", tables::dp_to_score(diff));
        let coef = tables::dp_to_score(diff);
       // println!("{: >25}Coeficiente [coef]: {}","",coef.unwrap()); 

        let coef_extract: f32 = coef.unwrap();  
        
    println!("{: >25}El coeficiente extraido: {:?}","", coef_extract); // imprimimos el vector completo




        elos_coef.push(coef_extract);
    }
    println!("{: >25}Elos de los oponentes son: {:?}","", elos); // imprimimos el vector completo
    println!("{: >25}Diferencia de Elos de los oponentes : {:?}","", elos_diff); // imprimimos el vector completo
    println!("{: >25}Puntos de los encuentros: {:?}","", elos_score); // imprimimos el vector completo
    println!("{: >25}El coeficiente respecto a los oponentes: {:?}","", elos_coef); // imprimimos el vector completo

//      for (item_score, item_coef) in elos_score.iter().zip(elos_coef.iter()) {
//  let item_elos_inc = *elos_score * *elos_coef;
// elos_inc.push(item_elos_inc); 
//  }
    let k: f32 = 20.0;
    for item in &elos_coef {
        elos_inc.push(*item * k) ;
    }

    println!("{: >25}El coeficiente * 100: {:?}","", elos_coef); // imprimimos el vector completo

    println!("{: >25}El coeficiente respecto a los puntos obtenidos: {:?}","", elos_inc); // imprimimos el vector completo
    println!("{: >25}El Incremento o decremento de puntos {:?}","", elos_inc); // imprimimos el vector completo
    println!("{: >25}Elos Relativos ganados o perdidos  {:?}","", elos_inc); // imprimimos el vector completo
    println!("{: >25}Elo Final {:?}","", elos_inc); // imprimimos el vector completo
    
    let vector_n = elos.len(); // obtenemos la cantidad de elementos del vector

    //println!("{: >25}Elementos de Vector n: {:?}","", vector_n); // imprimimos el vector completo
 // Hacer un bucle de las diferentes diferencias y sacar el tanto por ciento para sacar el incrmento de Elo
}

