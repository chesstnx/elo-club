
//#![allow(warnings)] 
struct Ratings {
    d_min: i32,  // D, into scoring probability
    d_max: i32,  // D, into scoring probability
    dp_max: f32, // PD, for the higher, H, and the lower, L,
    dp_min: f32, // Expresado en  100 y no en base decimal. hacer la conversion en la misma ecuacion.
}

struct Perfomance {
    p: f32,  // puntaje fraccional.
    dp: i32, //  diferencias de rating
}

// 8.1.2 Tabla de conversión de diferencia en la calificación, D (d), en la probabilidad de puntuación PD (pd), para el jugador con calificación más alta, H (dp_max) y más baja, L (dp_min), respectivamente.

pub fn dp_to_score(diff: i32) -> Option<f32> {
    let rating2score = vec![
        Ratings {
            d_min: 0,
            d_max: 3,
            dp_max: 0.50,
            dp_min: 0.50,
        },
        Ratings {
            d_min: 4,
            d_max: 10,
            dp_max: 0.51,
            dp_min: 0.49,
        },
        Ratings {
            d_min: 11,
            d_max: 17,
            dp_max: 0.52,
            dp_min: 0.48,
        },
        Ratings {
            d_min: 18,
            d_max: 25,
            dp_max: 0.53,
            dp_min: 0.47,
        },
        Ratings {
            d_min: 26,
            d_max: 32,
            dp_max: 0.54,
            dp_min: 0.46,
        },
        Ratings {
            d_min: 33,
            d_max: 39,
            dp_max: 0.55,
            dp_min: 0.45,
        },
        Ratings {
            d_min: 40,
            d_max: 46,
            dp_max: 0.56,
            dp_min: 0.44,
        },
        Ratings {
            d_min: 47,
            d_max: 53,
            dp_max: 0.57,
            dp_min: 0.43,
        },
        Ratings {
            d_min: 54,
            d_max: 61,
            dp_max: 0.58,
            dp_min: 0.42,
        },
        Ratings {
            d_min: 62,
            d_max: 68,
            dp_max: 0.59,
            dp_min: 0.41,
        },
        Ratings {
            d_min: 69,
            d_max: 76,
            dp_max: 0.60,
            dp_min: 0.40,
        },
        Ratings {
            d_min: 77,
            d_max: 83,
            dp_max: 0.61,
            dp_min: 0.39,
        },
        Ratings {
            d_min: 84,
            d_max: 91,
            dp_max: 0.62,
            dp_min: 0.38,
        },
        Ratings {
            d_min: 92,
            d_max: 98,
            dp_max: 0.63,
            dp_min: 0.37,
        },
        Ratings {
            d_min: 99,
            d_max: 106,
            dp_max: 0.64,
            dp_min: 0.36,
        },
        Ratings {
            d_min: 107,
            d_max: 113,
            dp_max: 0.65,
            dp_min: 0.35,
        },
        Ratings {
            d_min: 114,
            d_max: 121,
            dp_max: 0.66,
            dp_min: 0.34,
        },
        Ratings {
            d_min: 122,
            d_max: 129,
            dp_max: 0.67,
            dp_min: 0.33,
        },
        Ratings {
            d_min: 130,
            d_max: 137,
            dp_max: 0.68,
            dp_min: 0.32,
        },
        Ratings {
            d_min: 138,
            d_max: 145,
            dp_max: 0.69,
            dp_min: 0.31,
        },
        Ratings {
            d_min: 146,
            d_max: 153,
            dp_max: 0.70,
            dp_min: 0.30,
        },
        Ratings {
            d_min: 154,
            d_max: 162,
            dp_max: 0.71,
            dp_min: 0.29,
        },
        Ratings {
            d_min: 163,
            d_max: 170,
            dp_max: 0.72,
            dp_min: 0.28,
        },
        Ratings {
            d_min: 171,
            d_max: 179,
            dp_max: 0.73,
            dp_min: 0.27,
        },
        Ratings {
            d_min: 180,
            d_max: 188,
            dp_max: 0.74,
            dp_min: 0.26,
        },
        Ratings {
            d_min: 189,
            d_max: 197,
            dp_max: 0.75,
            dp_min: 0.25,
        },
        Ratings {
            d_min: 198,
            d_max: 206,
            dp_max: 0.76,
            dp_min: 0.24,
        },
        Ratings {
            d_min: 207,
            d_max: 215,
            dp_max: 0.77,
            dp_min: 0.23,
        },
        Ratings {
            d_min: 216,
            d_max: 225,
            dp_max: 0.78,
            dp_min: 0.22,
        },
        Ratings {
            d_min: 226,
            d_max: 235,
            dp_max: 0.79,
            dp_min: 0.21,
        },
        Ratings {
            d_min: 236,
            d_max: 245,
            dp_max: 0.80,
            dp_min: 0.20,
        },
        Ratings {
            d_min: 246,
            d_max: 256,
            dp_max: 0.81,
            dp_min: 0.19,
        },
        Ratings {
            d_min: 257,
            d_max: 267,
            dp_max: 0.82,
            dp_min: 0.18,
        },
        Ratings {
            d_min: 268,
            d_max: 278,
            dp_max: 0.83,
            dp_min: 0.17,
        },
        Ratings {
            d_min: 279,
            d_max: 290,
            dp_max: 0.84,
            dp_min: 0.16,
        },
        Ratings {
            d_min: 291,
            d_max: 302,
            dp_max: 0.85,
            dp_min: 0.15,
        },
        Ratings {
            d_min: 303,
            d_max: 315,
            dp_max: 0.86,
            dp_min: 0.14,
        },
        Ratings {
            d_min: 316,
            d_max: 328,
            dp_max: 0.87,
            dp_min: 0.13,
        },
        Ratings {
            d_min: 329,
            d_max: 344,
            dp_max: 0.88,
            dp_min: 0.12,
        },
        Ratings {
            d_min: 345,
            d_max: 357,
            dp_max: 0.89,
            dp_min: 0.11,
        },
        Ratings {
            d_min: 358,
            d_max: 374,
            dp_max: 0.90,
            dp_min: 0.10,
        },
        Ratings {
            d_min: 375,
            d_max: 391,
            dp_max: 0.91,
            dp_min: 0.09,
        },
        Ratings {
            d_min: 392,
            d_max: 411,
            dp_max: 0.92,
            dp_min: 0.08,
        },
        Ratings {
            d_min: 412,
            d_max: 432,
            dp_max: 0.93,
            dp_min: 0.07,
        },
        Ratings {
            d_min: 433,
            d_max: 456,
            dp_max: 0.94,
            dp_min: 0.06,
        },
        Ratings {
            d_min: 457,
            d_max: 484,
            dp_max: 0.95,
            dp_min: 0.05,
        },
        Ratings {
            d_min: 485,
            d_max: 517,
            dp_max: 0.96,
            dp_min: 0.04,
        },
        Ratings {
            d_min: 518,
            d_max: 559,
            dp_max: 0.97,
            dp_min: 0.03,
        },
        Ratings {
            d_min: 560,
            d_max: 619,
            dp_max: 0.98,
            dp_min: 0.02,
        },
        Ratings {
            d_min: 620,
            d_max: 735,
            dp_max: 0.99,
            dp_min: 0.01,
        },
        Ratings {
            d_min: 736,
            d_max: 999,
            dp_max: 100.0,
            dp_min: 0.0,
        },
    ];

 //   let absolute: bool;
    //let diff_abs = diff.abs();
    let absolute = diff.abs() == diff;

            //println!("[diff] : {}, [diff_abs]: {}, [absolute] : {}", diff, diff_abs, absolute); 

   //println!("Valor de la diferencia es [absolute]: {:?}", absolute); 
    for rating in rating2score {
        if (&diff >= &rating.d_min && &diff <= &rating.d_max) && absolute == true {
            //println!("Estado [absolute]: {:?}", absolute); 
            return Some(rating.dp_max);
        } else {
            if (&diff.abs() >= &rating.d_min && &diff.abs() <= &rating.d_max) && absolute == false {
            //println!("Estado [absolute]: {:?}", absolute); 
                return Some(rating.dp_min);
            }
        };
    }
    Some(0.0)
}

// La tabla de conversión de puntaje fraccionado, p, en diferencias de rating, dp

pub fn p2dp(p: f32) -> Option<i32> {
    let score2rating = vec![
        Perfomance { p: 1.0, dp: 800 },
        Perfomance { p: 0.99, dp: 677 },
        Perfomance { p: 0.98, dp: 589 },
        Perfomance { p: 0.97, dp: 538 },
        Perfomance { p: 0.96, dp: 501 },
        Perfomance { p: 0.95, dp: 470 },
        Perfomance { p: 0.94, dp: 444 },
        Perfomance { p: 0.93, dp: 422 },
        Perfomance { p: 0.92, dp: 401 },
        Perfomance { p: 0.91, dp: 383 },
        Perfomance { p: 0.90, dp: 366 },
        Perfomance { p: 0.87, dp: 322 },
        Perfomance { p: 0.86, dp: 309 },
        Perfomance { p: 0.85, dp: 296 },
        Perfomance { p: 0.84, dp: 284 },
        Perfomance { p: 0.83, dp: 273 },
        Perfomance { p: 0.82, dp: 262 },
        Perfomance { p: 0.81, dp: 251 },
        Perfomance { p: 0.80, dp: 240 },
        Perfomance { p: 0.79, dp: 230 },
        Perfomance { p: 0.78, dp: 220 },
        Perfomance { p: 0.77, dp: 211 },
        Perfomance { p: 0.76, dp: 202 },
        Perfomance { p: 0.75, dp: 193 },
        Perfomance { p: 0.74, dp: 184 },
        Perfomance { p: 0.73, dp: 175 },
        Perfomance { p: 0.72, dp: 166 },
        Perfomance { p: 0.71, dp: 158 },
        Perfomance { p: 0.70, dp: 149 },
        Perfomance { p: 0.69, dp: 141 },
        Perfomance { p: 0.68, dp: 133 },
        Perfomance { p: 0.67, dp: 125 },
        Perfomance { p: 0.66, dp: 117 },
        Perfomance { p: 0.65, dp: 110 },
        Perfomance { p: 0.64, dp: 102 },
        Perfomance { p: 0.63, dp: 95 },
        Perfomance { p: 0.62, dp: 87 },
        Perfomance { p: 0.61, dp: 80 },
        Perfomance { p: 0.60, dp: 72 },
        Perfomance { p: 0.59, dp: 65 },
        Perfomance { p: 0.58, dp: 57 },
        Perfomance { p: 0.57, dp: 50 },
        Perfomance { p: 0.56, dp: 43 },
        Perfomance { p: 0.55, dp: 36 },
        Perfomance { p: 0.54, dp: 29 },
        Perfomance { p: 0.53, dp: 21 },
        Perfomance { p: 0.52, dp: 14 },
        Perfomance { p: 0.51, dp: 7 },
        Perfomance { p: 0.50, dp: 0 },
        Perfomance { p: 0.49, dp: -7 },
        Perfomance { p: 0.48, dp: -14 },
        Perfomance { p: 0.47, dp: -21 },
        Perfomance { p: 0.46, dp: -29 },
        Perfomance { p: 0.45, dp: -36 },
        Perfomance { p: 0.44, dp: -43 },
        Perfomance { p: 0.43, dp: -50 },
        Perfomance { p: 0.42, dp: -57 },
        Perfomance { p: 0.41, dp: -65 },
        Perfomance { p: 0.40, dp: -72 },
        Perfomance { p: 0.38, dp: -87 },
        Perfomance { p: 0.37, dp: -95 },
        Perfomance { p: 0.36, dp: -102 },
        Perfomance { p: 0.35, dp: -110 },
        Perfomance { p: 0.34, dp: -117 },
        Perfomance { p: 0.33, dp: -125 },
        Perfomance { p: 0.32, dp: -133 },
        Perfomance { p: 0.31, dp: -141 },
        Perfomance { p: 0.30, dp: -149 },
        Perfomance { p: 0.29, dp: -158 },
        Perfomance { p: 0.28, dp: -166 },
        Perfomance { p: 0.27, dp: -175 },
        Perfomance { p: 0.26, dp: -184 },
        Perfomance { p: 0.25, dp: -193 },
        Perfomance { p: 0.24, dp: -202 },
        Perfomance { p: 0.23, dp: -211 },
        Perfomance { p: 0.22, dp: -220 },
        Perfomance { p: 0.21, dp: -230 },
        Perfomance { p: 0.20, dp: -240 },
        Perfomance { p: 0.19, dp: -251 },
        Perfomance { p: 0.18, dp: -262 },
        Perfomance { p: 0.17, dp: -273 },
        Perfomance { p: 0.16, dp: -284 },
        Perfomance { p: 0.15, dp: -296 },
        Perfomance { p: 0.14, dp: -309 },
        Perfomance { p: 0.13, dp: -322 },
        Perfomance { p: 0.12, dp: -336 },
        Perfomance { p: 0.11, dp: -351 },
        Perfomance { p: 0.10, dp: -366 },
        Perfomance { p: 0.09, dp: -383 },
        Perfomance { p: 0.08, dp: -401 },
        Perfomance { p: 0.07, dp: -422 },
        Perfomance { p: 0.06, dp: -444 },
        Perfomance { p: 0.05, dp: -470 },
        Perfomance { p: 0.04, dp: -501 },
        Perfomance { p: 0.03, dp: -538 },
        Perfomance { p: 0.02, dp: -589 },
        Perfomance { p: 0.01, dp: -677 },
        Perfomance { p: 0.00, dp: -800 },
    ];

    for perfomance in score2rating {
        if perfomance.p == p {
            //        println!(" Score :{}, Diff Rating: {} ", perfomance.p, perfomance.dp)
            return Some(perfomance.dp);
        }
    }

    None
}
